export interface PlayerState {

    inventory: {
        keys: 0,
        treasures: 0,
        swords: 0,
        torches: 0,
        potions: 0,
    },
    health: 10,
    dexterity: 1,
    strength: 1,
    smartness: 1,

}

export const playerState: PlayerState = {
    inventory: {
        keys: 0,
        treasures: 0,
        swords: 0,
        torches: 0,
        potions: 0,
    },
    health: 10,
    dexterity: 1,
    strength: 1,
    smartness: 1,
};