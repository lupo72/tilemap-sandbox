import "phaser";
import {TilemapScene} from "./scenes/TilemapScene";
import {SpeechBubble} from "./scenes/SpeechBubble";
import {BendWavesPostFX} from "./BendWavesPostFX";
import {HueRotatePostFX} from "./HueRotatePostFX";

var config: GameConfig = {
    title: "BlockNinja Platform Dummy: a Phaser3 BoilerPlate",
    type: Phaser.WEBGL,
    width: 480,
    height: 600,
    backgroundColor: '#cecece',
    pixelArt: true,
    physics: {
        default: 'arcade',
        arcade: {
            // gravity: {x: 0, y: 0 },
            debug: true
        }
    },
    scene: [TilemapScene],
    // @ts-ignore
    pipeline: {BendWavesPostFX}
};


export class Game extends Phaser.Game {
    constructor(config: GameConfig) {
        super(config);
    }
}

window.onload = () => {
    var game = new Game(config);
};


// import "phaser";
// import { initialScene } from "./scenes/initialScene";
//
// /// <reference path="../../phaser.d.ts"/>
//
// const config: GameConfig = {
//     title: "Phaser3 BoilerPlate",
//     type: Phaser.AUTO,
//     width: 800,
//     height: 600,
//     scene: [initialScene],
//     input: {
//         keyboard: true,
//         mouse: false,
//         touch: false,
//         gamepad: false
//     }
// };
//
// export class Game extends Phaser.Game {
//     constructor(config: GameConfig) {
//       super(config);
//     }
// }
//
// window.onload = () => {
//     var game = new Game(config);
// };
