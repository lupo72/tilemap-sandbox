import * as dat from 'dat.gui';
import findPath from "../findPath";
import {SpeechBubble} from "./SpeechBubble";
import {BendWavesPostFX} from "../BendWavesPostFX";
import {HueRotatePostFX} from "../HueRotatePostFX";

export const BORDER_TILE = 17;

export const TILE_DIM = {width: 32, height: 32};

export interface PlayerState {

    inventory: {
        keys: number,
        treasures: number,
        swords: number,
        torches: number,
        potions: number,
    },
    health: number,
    dexterity: number,
    strength: number,
    smartness: number,
}

export class TilemapScene extends Phaser.Scene {

    pointer;
    player;
    nextX;
    nextY;
    text;
    isClicking = false;
    ghost = null;
    ghostDestroyed = true;
    speed = 120;
    map;
    private moveToTarget?: Phaser.Math.Vector2
    private movePath: Phaser.Math.Vector2[] = []
    private boden;
    private mauer;
    private objekte;
    distX;
    upX;
    distY;
    upY;
    dist;
    pi;
    threshold = 25;
    downX;
    downY;
    gui;
    value = "Foobar"
    playerState: PlayerState;
    rewspanSpeechBubble;
    speechBubbleWasOpen = false;
    cameraFx;

    constructor() {
        super({key: "TilemapScene"});
    }

    preload() {
        this.load.spritesheet('FantasyTileset', './src/assets/tilesets/fantasy-tileset_greyscaled.png', {
            frameWidth: TILE_DIM.width,
            frameHeight: TILE_DIM.height
        });
        this.load.tilemapTiledJSON('Dungeon', './src/assets/tilemaps/dungeon-01.json');
    }

    createWorld() {
        this.map = this.add.tilemap('Dungeon');
        const tileSet = this.map.addTilesetImage('FantasyTileset');
        this.boden = this.map.createLayer('Boden', tileSet, 0, 0);
        this.mauer = this.map.createLayer('Mauer', tileSet, 0, 0);
        this.objekte = this.map.createLayer('Objekte', tileSet, 0, 0);

        // Set Collisions
        this.mauer.setCollision(BORDER_TILE, false);
        this.physics.add.collider(this.player, this.mauer);
        this.mauer.setTileIndexCallback(BORDER_TILE, (gameObject) => {
        }, this);

        this.objekte.setCollisionBetween(33, 112, true);
        this.physics.add.collider(this.player, this.objekte);

        const t = [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
            46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
            57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67,
            68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78,
            79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
            90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
            101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
        ]

        // @ts-ignore
        this.objekte.setTileIndexCallback(t, (go1) => {
            this.playerState.inventory.treasures += 1;
            const tile = this.map.removeTileAtWorldXY(go1.x, go1.y);
            // console.log(tile);
            this.player.setTint(0x00ff00);

            this.cameras.main.resetPostPipeline();

            this.time.delayedCall(3000, () => {
                this.player.clearTint()
            }, [], this);
        }, this)

    }

    createPlayer() {
        this.player = this.physics.add.sprite(112, 140, 'FantasyTileset', 150);
        // this.player.setSize(16, 16);
        this.player.refreshBody();
        this.player.depth = 1;
        this.player.setTint(0xf48fb1);
    }

    createCreatures() {
        const npcCollection = this.map.getObjectLayer('NPCs');
        if (npcCollection) {
            // const NPCs = new Phaser.GameObjects.Group(this);
            for (let i = 0; i < npcCollection.objects.length; i += 1) {
                let npc = npcCollection.objects[i];
                // console.log(npc);
                // const tile = boden.getTileAtWorldXY(npc.x, npc.y);
                // console.log(tile);
                let sprite;
                let frame;

                // user properties sind ein Array von Objekten in der Form { name: PropName, value: propValue }
                const time = npc.properties.filter(item => item.name === "time")?.shift()?.value ?? 1000;
                const directionX = npc.properties.filter(item => item.name === "direction_x")?.shift()?.value;
                const directionY = npc.properties.filter(item => item.name === "direction_y")?.shift()?.value;
                const stepsX = npc.properties.filter(item => item.name === "steps_x")?.shift()?.value ?? 3;
                const stepsY = npc.properties.filter(item => item.name === "steps_y")?.shift()?.value ?? 3;
                const delay = npc.properties.filter(item => item.name === "tween_delay")?.shift()?.value ?? 3;
                const delayAndHold = npc.properties.filter(item => item.name === "tween_delay_and_hold")?.shift()?.value ?? 3;
                const delayHold = npc.properties.filter(item => item.name === "tween_hold")?.shift()?.value ?? 3;
                const dialog = npc.properties.filter(item => item.name === "dialog")?.shift()?.value ?? ['lorem', 'ipsum'];
                const npcX = npc.x + TILE_DIM.width / 2;
                const npcY = npc.y - TILE_DIM.height / 2;
                let tint = null;
                switch (npc.name) {
                    case 'Banshee':
                        frame = 174;
                        tint = 0xFFCC80;
                        break;

                    case "Bat":
                        frame = 163;
                        break;

                    case 'Beholder':
                        frame = 175;
                        break;

                    case 'Bunny':
                        frame = 161;
                        tint = 0xff8a65;
                        break;

                    case 'Hog':
                        frame = 166;
                        break;

                    case 'Scorpio':
                        frame = 170;
                        tint = 0xf44336;
                        break;

                    case 'Toady':
                        frame = 156;
                        tint = 0x689f38;
                        break;

                    case 'Vlad':
                        frame = 172;
                        tint = 0xffebee;
                }
                sprite = this.physics.add.sprite(npcX, npcY, 'FantasyTileset', frame);

                if (sprite) {
                    if (tint) {
                        sprite.setTint(tint);
                    } else {
                        const hsv = Phaser.Display.Color.HSVColorWheel();
                        const i = Phaser.Math.Between(0, 359);
                        // @ts-ignore
                        sprite.setTint(hsv[i].color);
                    }

                    // Add collider for nasty creatures

                    this.physics.add.existing(sprite);
                    this.physics.add.collider(sprite, this.player, (go1, go2) => {
                        // console.log("collide ", go1, go2);
                        this.player.setTint(0xff0000);
                        if (!this.scene.get('SpeechBubble') && !this.rewspanSpeechBubble || this.rewspanSpeechBubble < Date.now()) {
                            this.rewspanSpeechBubble = Date.now() + 5000;
                            this.speechBubbleWasOpen = true;
                            this.scene.add('SpeechBubble', SpeechBubble, true, {
                                x: 0,
                                y: 0,
                                text: dialog
                            });
                            // this.moveToTarget = null;
                            this.cameraFx = this.cameras.main.setPostPipeline(BendWavesPostFX);

                            this.scene.pause('TilemapScene');
                        }

                        this.time.delayedCall(3000, () => {
                            this.player.clearTint();
                        }, [], this);
                    }, null, this);

                    let yMovement = null;
                    if (directionY === "up" || directionY === "down") {
                        yMovement = directionX === "up" ? npc.y + (TILE_DIM.height * 1.5) * stepsY : npc.y - (TILE_DIM.height * 1.5) * stepsY;
                    }

                    let xMovement = null;
                    if (directionX === "left" || directionX === "right") {
                        xMovement = directionX === "left" ? npc.x - TILE_DIM.width * stepsX : npc.x + TILE_DIM.width * stepsX;
                    }
                    let config = {
                        targets: sprite,
                        loop: -1,
                        yoyo: true,
                        duration: time,
                    }

                    if (xMovement !== null) {
                        config['x'] = xMovement;
                    }

                    if (yMovement !== null) {
                        config['y'] = yMovement;
                    }

                    // initial delay
                    config['delay'] = delay;

                    // delay between tween and yoyo
                    config['hold'] = delayHold;

                    // delay to next pass
                    config['repeatDelay'] = delayAndHold;

                    // delay to next loop
                    config['loopDelay'] = delayAndHold;

                    this.add.tween(config);
                }
            }
        }
    }

    create() {
        this.createPlayer();
        this.createWorld();
        this.createCreatures();
        this.physics.world.setBounds(0, 0, this.map.width * TILE_DIM.width, this.map.height * TILE_DIM.height);
        this.cameras.main.setBounds(0, 0, this.map.width * TILE_DIM.width, this.map.height * TILE_DIM.height);
        this.cameras.main.startFollow(this.player).setZoom(1.5);

        // this.cameras.main.setPostPipeline(BendWavesPostFX);
        // this.cameras.main.setPostPipeline(HueRotatePostFX);

        this.input.on(Phaser.Input.Events.POINTER_DOWN, (pointer: Phaser.Input.Pointer) => {
            this.downX = pointer.x;
            this.downY = pointer.y;
        });

        this.input.on(Phaser.Input.Events.POINTER_UP, (pointer: Phaser.Input.Pointer) => {

            if (this.speechBubbleWasOpen && this.scene.get('SpeechBubble') === null) {
                this.speechBubbleWasOpen = false;
                return;
            }

            // console.log(this.scene.get('SpeechBubble'));

            const distX = pointer.x - this.downX;
            const distY = pointer.y - this.downY;

            // const dist = Math.sqrt(distX * distX + distY * distY);
            // const threshold = 25;
            // if (dist > threshold) {
            //  // Swipe event
            // }

            const {worldX, worldY} = pointer
            const startVec = this.boden.worldToTileXY(this.player.x, this.player.y)
            const targetVec = this.boden.worldToTileXY(worldX, worldY)
            // generate the path
            const path = findPath(startVec, targetVec, this.boden, this.mauer)
            // give it to the player to use
            this.moveAlong(path)

        })

        this.events.on(Phaser.Scenes.Events.SHUTDOWN, () => {
            this.input.off(Phaser.Input.Events.POINTER_UP)
        })

        this.playerState = {
            health: 10,
            dexterity: 1,
            strength: 1,
            smartness: 1,
            inventory: {
                keys: 0,
                treasures: 0,
                swords: 0,
                torches: 0,
                potions: 0,
            },
        }

        this.setupGUI();
    }

    setupGUI() {
        this.gui = new dat.GUI({'name': 'HUD'});
        var guiLevel = this.gui.addFolder('General');
        guiLevel.add(this, 'value', 0, 10).name('my Value');
        // guiLevel.add(this, 'listen', 0, 100).name('listen').listen();
        // guiLevel.add(this, 'randomColor').name('randomColor');
        const gui2 = this.gui.addFolder('Player');
        gui2.add(this.player, 'x')
        gui2.add(this.playerState.inventory, 'keys')
        gui2.add(this.playerState.inventory, 'treasures')
        guiLevel.open();
    }

    getPlayerHealth() {
        return this.playerState.health;
    }

    moveAlong(path: Phaser.Math.Vector2[]) {
        if (!path || path.length <= 0) {
            return
        }

        this.movePath = path
        this.moveTo(this.movePath.shift()!)
    }

    moveTo(target: Phaser.Math.Vector2) {
        this.moveToTarget = target
        const tile = this.boden.getTileAtWorldXY(target.x, target.y);
    }

    update() {
        let dx = 0
        let dy = 0

        if (this.moveToTarget) {
            const playerX = this.player.x;
            const playerY = this.player.y;

            dx = this.moveToTarget.x - playerX
            dy = this.moveToTarget.y - playerY
            const targetTile = this.map.worldToTileXY(this.moveToTarget.x, this.moveToTarget.y);
            const currentTile = this.map.worldToTileXY(playerX, playerY);
            if (Math.abs(dx) < 5) {
                dx = 0
            }
            if (Math.abs(dy) < 5) {
                dy = 0
            }

            if (currentTile.x === targetTile.x && currentTile.y === targetTile.y) { //

                if (this.movePath.length > 0) {
                    this.moveTo(this.movePath.shift()!)
                    return
                }

                this.moveToTarget = undefined
                // this.boden.putTileAtWorldXY(2, playerX, playerY);
            }


        }

        const leftDown = dx < 0
        const rightDown = dx > 0
        const upDown = dy < 0
        const downDown = dy > 0

        if (leftDown) {
            this.player.x -= 4;
            this.player.flipX = true
        }
        if (rightDown) {
            this.player.x += 4;
            this.player.flipX = false
        }
        if (upDown) {
            this.player.y -= 4;
        }
        if (downDown) {
            this.player.y += 4;
        }

        if (dx === 0 && dy === 0) {
            this.moveToTarget = undefined
        }

        this.gui.updateDisplay();

        this.physics.world.overlapTiles(this.player, this.objekte, this.hitObjekt, null, this);
    }

    hitObjekt(player, tile) {
        this.objekte = this.map.filterTiles(function (tile) {
            console.log(tile?.index);
            //return (tile.index === 82);
        });
    }
}
