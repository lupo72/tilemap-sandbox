export class SpeechBubble extends Phaser.Scene {
    constructor() {
        super({key: "SpeechBubble"});
    }

    create(attrs) {
        let width = this.cameras.main.width;
        let height = this.cameras.main.height;
        let box = this.add.graphics();
        box.fillStyle(0x222222, 0.8);
        let boxX = this.cameras.main.worldView.x + 20;
        let boxY = this.cameras.main.worldView.y + 30;
        let boxW = this.cameras.main.width - 40;
        let boxH = 250;
        box.fillRect(boxX, boxY, boxW, boxH);
        this.add.text(100, 100, attrs.text, { fontFamily: 'Arial', color: '#00ff00' });
        // let label = this.make.text({
        //     x: boxX + width / 2,
        //     y: boxY + 8,
        //     text: attrs.text,
        //     style: {
        //         font: '20px monospace',
        //         // backgroundColor: '#FFFFFF',
        //     }
        // });
        // label.setOrigin(0.5, 0.5);

        // Activate This Part when moving on to RPG
        // (maybe i'll use something like this as Template for "In Game Fight Scene" later)
        // https://github.com/photonstorm/phaser/issues/4194
        box.setInteractive(new Phaser.Geom.Rectangle(boxX, boxY, boxW, boxH), Phaser.Geom.Rectangle.Contains);
        box.on('pointerdown', function(){
            this.scene.resume('TilemapScene');
            this.scene.remove('SpeechBubble');
        }, this);
    }
}


