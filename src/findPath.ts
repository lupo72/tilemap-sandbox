// s. https://www.thepolyglotdeveloper.com/2020/09/include-touch-cursor-gesture-events-phaser-game/

import {TILE_DIM} from "./scenes/TilemapScene";

interface TilePosition
{
    x: number,
    y: number
}

const toKey = (x: number, y: number) => `${x}x${y}`

const findPath = (start: Phaser.Math.Vector2, target: Phaser.Math.Vector2, groundLayer: Phaser.Tilemaps.StaticTilemapLayer, wallsLayer: Phaser.Tilemaps.StaticTilemapLayer) => {
    // no path if select invalid tile
    if (!groundLayer.getTileAt(target.x, target.y))
    {
        return []
    }

    // no path if select a wall
    if (wallsLayer.getTileAt(target.x, target.y))
    {
        return []
    }

    const queue: TilePosition[] = []
    const parentForKey: { [key: string]: { key: string, position: TilePosition } } = {}

    const startKey = toKey(start.x, start.y)
    const targetKey = toKey(target.x, target.y)

    parentForKey[startKey] = {
        key: '',
        position: { x: -1, y: -1 }
    }

    // groundLayer.tilemap.putTileAt(4, target.x, target.y);
    queue.push(start)

    while (queue.length > 0)
    {
        const { x, y } = queue.shift()!
        const currentKey = toKey(x, y)
        if (currentKey === targetKey)
        {
            break
        }

        const neighbors = [
            { x, y: y - 1 },	// top
            { x: x + 1, y }, 	// right
            { x: x - 1, y},		// left
            { x, y: y + 1 },	// bottom
            // { x: x - 1, y: y - 1 },	// top left
            // { x: x + 1, y: y - 1 },	// top right
            // { x: x - 1, y: y + 1 },	// bottom left
            // { x: x + 1, y: y + 1 },	// bottom right
        ]

        neighbors.forEach( neighbor => {
            const tile = groundLayer.getTileAt(neighbor.x, neighbor.y)
            // console.log('checking floor tile', tile);
            if (!tile)
            {
                return;
            }

            const wall = wallsLayer.getTileAt(neighbor.x, neighbor.y);

            if (wall)
            {
                return;
            }

            const key = toKey(neighbor.x, neighbor.y)

            if (key in parentForKey)
            {
                return;
            }

            parentForKey[key] = {
                key: currentKey,
                position: { x, y }
            }
            // console.log("neighbor", neighbor);
            queue.push(neighbor)
        });
    }

    // queue.push(target);

    const path: Phaser.Math.Vector2[] = [groundLayer.tileToWorldXY(target.x, target.y)]

    let currentKey = targetKey
    let currentPos = parentForKey[targetKey].position

    while (currentKey !== startKey)
    {
        const pos = groundLayer.tileToWorldXY(currentPos.x, currentPos.y)
        // Place Position into Middle of Tile
        pos.x += groundLayer.tilemap.tileWidth * 0.5;
        pos.y += groundLayer.tilemap.tileHeight * 0.5;
        // groundLayer.tilemap.putTileAtWorldXY(3, pos.x, pos.y);
        path.push(pos)
        const { key, position } = parentForKey[currentKey]
        currentKey = key
        currentPos = position
    }

    // const pos = groundLayer.tileToWorldXY(target.x, target.y);
    // path.push(pos);
    // path.forEach(p => console.log(p));

    return path.reverse();

}

export default findPath